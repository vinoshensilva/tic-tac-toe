// Import libraries
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { store } from '@/store/index';

// Import styles
import '../styles/globals.css';

// Import components
import Modal from '@/components/molecule/modal/index';

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Modal />
      <Component {...pageProps} />
    </Provider>
  );
}

MyApp.propTypes = {
  pageProps: PropTypes.shape({}).isRequired,
  Component: PropTypes.elementType.isRequired,
};

export default MyApp;
