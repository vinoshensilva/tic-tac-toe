// Import libraries
import { useSelector } from 'react-redux';

// Import components
import Grids from '@/components/organism/grids/index';

export default function Home() {
  const currentTurn = useSelector((state) => state.gridReducer.currentTurn);
  const playerXScore = useSelector((state) => state.gridReducer.playerXScore);
  const playerOScore = useSelector((state) => state.gridReducer.playerOScore);

  function renderTurn() {
    return (
      <div className="container__score">
        <h3>
          Current Turn:
          {` ${currentTurn}`}
        </h3>
      </div>
    );
  }

  function renderScores() {
    return (
      <>
        <div className="container__score">
          <h3>
            X Score:
            {' '}
            {playerXScore}
          </h3>
        </div>
        <div className="container__score">
          <h3>
            O Score:
            {' '}
            {playerOScore}
          </h3>
        </div>
      </>
    );
  }

  return (
    <div className="container">
      <div>
        {renderScores()}
        {renderTurn()}
      </div>
      <Grids />
    </div>
  );
}
