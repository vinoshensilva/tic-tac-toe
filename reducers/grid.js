/* eslint-disable no-param-reassign */

// Import libraries
import { createSlice } from '@reduxjs/toolkit';

// Import constants
import {
  DRAW,
  MARK_O,
  MARK_X,
  DRAW_SCORE_KEY,
  PLAYER_O_SCORE_KEY,
  PLAYER_X_SCORE_KEY,
} from '@/constants/grid';

import { checkStaleMate, checkWinner } from '@/functions/grid';
import { save } from '@/functions/storage';

const initialState = {
  grid: Array(9).fill(''),
  currentTurn: MARK_X,
  winner: '',
  playerXScore: 0,
  playerOScore: 0,
  drawScore: 0,
};

const gridSlice = createSlice({
  name: 'grid',
  initialState,
  reducers: {
    setGridCell: (state, action) => {
      const {
        index,
      } = action.payload;

      if (state.winner) return;

      // If the cell is already marked
      if (state.grid[index]) return;

      const copyGrid = [...state.grid];

      copyGrid[index] = state.currentTurn;

      state.currentTurn = state.currentTurn === MARK_O ? MARK_X : MARK_O;

      const winner = checkWinner(copyGrid, 0, 1, 2)
            || checkWinner(copyGrid, 3, 4, 5)
            || checkWinner(copyGrid, 6, 7, 8)
            || checkWinner(copyGrid, 0, 3, 6)
            || checkWinner(copyGrid, 1, 4, 7)
            || checkWinner(copyGrid, 2, 5, 8)
            || checkWinner(copyGrid, 0, 4, 8)
            || checkWinner(copyGrid, 6, 4, 2)
            || checkStaleMate(copyGrid);

      if (winner) {
        state.winner = winner;

        switch (winner) {
          case MARK_X:
            state.playerXScore += 1;
            save(PLAYER_X_SCORE_KEY, state.drawScore);
            break;
          case MARK_O:
            state.playerOScore += 1;
            save(PLAYER_O_SCORE_KEY, state.drawScore);
            break;
          case DRAW:
            state.drawScore += 1;
            save(DRAW_SCORE_KEY, state.drawScore);
            break;
          default:
            break;
        }
      }

      state.grid = copyGrid;
    },
    reset: (state) => ({
      ...initialState,
      playerXScore: state.playerXScore,
      playerOScore: state.playerOScore,
      drawScore: state.drawScore,
    }),
  },
});

export const {
  setGridCell,
  reset,
} = gridSlice.actions;

export default gridSlice.reducer;
