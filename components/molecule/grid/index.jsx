// Import libraries
import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

// Import components
import Button from '@/components/atom/button/index';

// Import constants
import { MARK_O, MARK_X } from '@/constants/grid';

// Import actions
import { setGridCell } from '@/reducers/grid';

function Grid({ mark, index }) {
  const dispatch = useDispatch();

  const onClick = () => {
    dispatch(setGridCell({ index }));
  };

  return (
    <div className="grids__grid">
      <Button
        onClick={onClick}
        className={`grids__button ${mark === MARK_X ? ' fuschia-color ' : ' black-color '}`}
      >
        {mark}
      </Button>
    </div>
  );
}

Grid.defaultProps = {
  mark: '',
};

Grid.propTypes = {
  index: PropTypes.number.isRequired,
  mark: PropTypes.oneOf([MARK_O, MARK_X, '']),
  ...Button.propTypes,
};

export default Grid;
