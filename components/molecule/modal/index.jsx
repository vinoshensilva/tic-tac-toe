// Import libraries
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Import components
import Button from '@/components/atom/button/index';

// Import reducers
import { reset } from '@/reducers/grid';
import { MARK_O, MARK_X } from '@/constants/grid';

function Modal() {
  const winner = useSelector((state) => state.gridReducer.winner);

  const dispatch = useDispatch();

  const onClick = () => {
    dispatch(reset());
  };

  const getWinText = () => {
    if (winner) {
      if ([MARK_O, MARK_X].includes(winner)) {
        return `${winner} WON !!!`;
      }

      return 'DRAW';
    }

    return '';
  };

  if (!winner) {
    return undefined;
  }

  return (
    <div className="modal">
      <div>
        <div className="container__winner">
          <h1>
            {getWinText()}
          </h1>

          <Button
            onClick={onClick}
            className="modal__restartbtn"
          >
            Restart
          </Button>
        </div>
      </div>
    </div>
  );
}

export default Modal;
