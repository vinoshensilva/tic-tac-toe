// Import libraries
import React from 'react';
import { useSelector } from 'react-redux';

// Import components
import Grid from '@/components/molecule/grid/index';

function Grids() {
  const grid = useSelector((state) => state.gridReducer.grid);

  return (
    <div
      className="grids"
    >
      {
          grid.map((mark, index) => (
            <Grid
              // eslint-disable-next-line react/no-array-index-key
              key={index}
              mark={mark}
              index={index}
            />
          ))
      }
    </div>
  );
}

export default Grids;
