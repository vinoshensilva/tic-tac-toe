function saveData(key, data) {
  if (typeof window !== 'undefined') {
    localStorage.setItem(key, JSON.stringify(data));
  }
}

function loadData(key) {
  if (typeof window !== 'undefined') {
    const data = localStorage.getItem(key);

    return data ? JSON.parse(data) : null;
  }

  return undefined;
}

export const save = saveData;
export const load = loadData;
