import { DRAW } from '@/constants/grid';

export function checkWinner(grid, p1, p2, p3) {
  if (grid[p1]) {
    if (grid[p1] === grid[p2] && grid[p2] === grid[p3]) {
      return grid[p1];
    }
  }

  return '';
}

export function checkStaleMate(grid = []) {
  const { length } = grid;

  let isStalemate = true;

  for (let i = 0; i < length; i += 1) {
    if (!grid[i]) {
      isStalemate = false;
    }
  }

  return isStalemate ? DRAW : '';
}
