import { configureStore } from '@reduxjs/toolkit';

// Import reducers
import gridReducer from '@/reducers/grid';

export const store = configureStore({
  reducer: {
    gridReducer,
  },
});
