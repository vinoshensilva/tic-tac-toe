export const MARK_X = 'X';
export const MARK_O = 'O';

export const DRAW = 'DRAW';

export const DRAW_SCORE_KEY = 'DRAW_SCORE_KEY';
export const PLAYER_O_SCORE_KEY = 'PLAYER_O_SCORE_KEY';
export const PLAYER_X_SCORE_KEY = 'PLAYER_X_SCORE_KEY';
